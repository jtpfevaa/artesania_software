= Artesanía del software
Ibon Urrutia <urrutia.ibon@gmail.com>
:icons: font
:toc:
:revealjs: https://revealjs.com/#/[reveal.js]
:asciidoc-revealjs: https://github.com/asciidoctor/asciidoctor-reveal.js/[Asciidoctor reveal.js converter]
:asciidoc: https://asciidoctor.org[Asciidoctor]
:asciidoctor-diagrams: https://asciidoctor.org/docs/asciidoctor-diagram/[Asciidoctor diagram]
:craftman: https://en.wikipedia.org/wiki/Software_craftsmanship[Software Craftmanship]

A {revealjs} presentation about modern {craftman}.

== Usage
- Clone this repository
+
[code,bash]
----
$ git clone git@gitlab.com:ibon-urrutia-presentations/artesania_software.git
----

=== Presentation

- Build presentation with
+
[code,bash]
----
$ cd artesania_software
$ gradle asciidoc
----
- Open `build/asciidoc/revealjs/index.html` in a browser
- For creating a zip file distribution build it with:
+
[code,bash]
----
$ gradle presentation
----

=== AddMoney application

- Build the application with
+
[code,bash]
----
$ cd artesania_software
$ gradle build
----
- The application is provided as `build/distributions/addMoney-3.0.0.tar` or `build/distributions/addMoney-3.0.0.zip`
- Uncompres and run `addMoney-3.0.0/bin/addMoney.sh` or `addMoney-3.0.0/bin/addMoney.bat`

== Bibliography and References
Check link:extra/artesania_software_referencias.adoc[References]